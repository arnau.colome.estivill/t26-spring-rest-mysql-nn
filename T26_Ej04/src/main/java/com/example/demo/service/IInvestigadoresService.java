package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.Investigadores;

public interface IInvestigadoresService {

	public List<Investigadores> listarInvestigadores(); 
	
	public Investigadores guardarInvestigador(Investigadores investigador);
	
	public Investigadores investigadorXID(String id);
	
	public Investigadores actualizarInvestigador(Investigadores investigador);
	
	public void eliminarInvestigador(String id);
	

	
}
