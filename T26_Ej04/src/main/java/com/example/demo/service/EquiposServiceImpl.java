package com.example.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.IEquiposDAO;
import com.example.demo.dto.Equipos;

@Service
public class EquiposServiceImpl implements IEquiposService{
	

	@Autowired
	IEquiposDAO iEquiposDAO;
	
	@Override
	public List<Equipos> listarEquipos() {
		return iEquiposDAO.findAll();
	}

	@Override
	public Equipos guardarEquipo(Equipos equipo) {
		return iEquiposDAO.save(equipo);
	}

	@Override
	public Equipos equipoXID(char id) {
		return iEquiposDAO.findById(id).get();
	}

	@Override
	public Equipos actualizarEquipo(Equipos equipo) {
		return iEquiposDAO.save(equipo);
	}

	@Override
	public void eliminarEquipo(char id) {
		iEquiposDAO.deleteById(id);
	}
}
