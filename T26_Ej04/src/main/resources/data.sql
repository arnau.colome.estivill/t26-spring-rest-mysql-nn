DROP DATABASE IF EXISTS T26_04;
CREATE DATABASE IF NOT EXISTS T26_04;
USE T26_04;

CREATE TABLE facultad(
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre NVARCHAR(100)
);

INSERT INTO facultad (nombre) VALUES ('Facultad Reus'), ('Facultad Tarragona'), ('Facultad Barcelona'), ('Facultad Lleida'), ('Facultad Bilbao');


CREATE TABLE equipos(
	numserie CHAR(4) PRIMARY KEY,
    nombre NVARCHAR(100),
    facultad INT,
    FOREIGN KEY (facultad) REFERENCES facultad(id)
);

INSERT INTO equipos VALUES ('1', 'Equipo 1', 1), ('2', 'Equipo 2', 2), ('3', 'Equipo 3', 3), ('4', 'Equipo 4', 4), ('5', 'Equipo 5', 5);


CREATE TABLE investigadores(
	dni VARCHAR(8) PRIMARY KEY,
    nomapels NVARCHAR(255),
    facultad INT,
    FOREIGN KEY (facultad) REFERENCES facultad(id)
);

INSERT INTO investigadores VALUES ('16234452', 'Juan Antonio Lopez Vega', 1), ('46122589', 'Jose Carlo Magno Segundo', 2), ('78523916', 'Pedro Picapiedra Rodríguez', 3), ('19485627', 'Nuria Roca Moreno', 4), ('13467956', 'Maria del Mar Sofia LLoria', 5);


CREATE TABLE reserva(
	id INT AUTO_INCREMENT PRIMARY KEY,
	dni VARCHAR(8),
	numserie CHAR(4),
	comienzo DATE,
    fin DATE,
	FOREIGN KEY (dni) REFERENCES investigadores(dni),
    FOREIGN KEY (numserie) REFERENCES equipos(numserie)
);

INSERT INTO reserva (dni, numserie, comienzo, fin) VALUES ('16234452', '1', '2018-5-10', '2020-08-5'), ('46122589', '2', '2016-02-25', '2018-06-25'), ('78523916', '3', '2019-02-2', '2020-02-2'), ('19485627', '4', '2020-02-3', '2020-03-2'), ('13467956', '5', '2010-10-10', '2020-10-10');

