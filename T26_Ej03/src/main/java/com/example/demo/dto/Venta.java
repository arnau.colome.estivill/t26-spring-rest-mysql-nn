package com.example.demo.dto;

import javax.persistence.*;

@Entity
@Table(name="venta")
public class Venta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) //busca ultimo valor e incrementa desde id final de db
	@Column(name="id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="cajero")
	private Cajeros cajero;
	
	@ManyToOne
	@JoinColumn(name="maquina")
	private Maquinas maquina;
	
	@ManyToOne
	@JoinColumn(name="producto")
	private Productos producto;

	public Venta() {
		
	}

	/**
	 * @param id
	 * @param cajero
	 * @param maquina
	 * @param producto
	 */
	public Venta(int id, Cajeros cajero, Maquinas maquina, Productos producto) {
		super();
		this.id = id;
		this.cajero = cajero;
		this.maquina = maquina;
		this.producto = producto;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the cajero
	 */
	public Cajeros getCajero() {
		return cajero;
	}

	/**
	 * @param cajero the cajero to set
	 */
	public void setCajero(Cajeros cajero) {
		this.cajero = cajero;
	}

	/**
	 * @return the maquina
	 */
	public Maquinas getMaquina() {
		return maquina;
	}

	/**
	 * @param maquina the maquina to set
	 */
	public void setMaquina(Maquinas maquina) {
		this.maquina = maquina;
	}

	/**
	 * @return the producto
	 */
	public Productos getProducto() {
		return producto;
	}

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Productos producto) {
		this.producto = producto;
	}


	@Override
	public String toString() {
		return "RegistroCurso [id=" + id + ", cajero=" + cajero + ", maquina=" + maquina+ ", producto="	+ producto+ "]";
	}

	
}
