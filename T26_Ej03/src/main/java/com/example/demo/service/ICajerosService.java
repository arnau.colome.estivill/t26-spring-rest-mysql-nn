package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.Cajeros;

public interface ICajerosService {

	//Metodos del CRUD
	public List<Cajeros> listarCajeros(); //Listar All 
	
	public Cajeros guardarCajeros(Cajeros cajeros);	//Guarda un Cajeros CREATE
	
	public Cajeros cajerosXID(int id); //Leer datos de un Cajeros READ
	
	public Cajeros actualizarCajeros(Cajeros cajeros); //Actualiza datos del Cajeros UPDATE
	
	public void eliminarCajeros(int id);// Elimina el Cajeros DELETE

	
}
