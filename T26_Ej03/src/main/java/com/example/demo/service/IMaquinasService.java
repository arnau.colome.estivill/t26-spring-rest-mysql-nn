package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.Maquinas;

public interface IMaquinasService {

	//Metodos del CRUD
	public List<Maquinas> listarMaquinas(); //Listar All 
	
	public Maquinas guardarMaquinas(Maquinas maquinas);	//Guarda un Maquinas CREATE
	
	public Maquinas maquinasXID(int id); //Leer datos de un Maquinas READ
	
	public Maquinas actualizarMaquinas(Maquinas maquinas); //Actualiza datos del Maquinas UPDATE
	
	public void eliminarMaquinas(int id);// Elimina el Maquinas DELETE

	
}
