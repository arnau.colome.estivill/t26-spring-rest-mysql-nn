package com.example.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.IMaquinasDAO;
import com.example.demo.dto.Maquinas;

@Service
public class MaquinasServiceImpl implements IMaquinasService{
	
	@Autowired
	IMaquinasDAO iMaquinasDAO;

	@Override
	public List<Maquinas> listarMaquinas() {
		return iMaquinasDAO.findAll();
	}

	@Override
	public Maquinas guardarMaquinas(Maquinas maquinas) {
		return iMaquinasDAO.save(maquinas);
	}

	@Override
	public Maquinas maquinasXID(int id) {
		return iMaquinasDAO.findById(id).get();
	}

	@Override
	public Maquinas actualizarMaquinas(Maquinas Maquinas) {
		return iMaquinasDAO.save(Maquinas);
	}

	@Override
	public void eliminarMaquinas(int id) {
		iMaquinasDAO.deleteById(id);
	}

	
}