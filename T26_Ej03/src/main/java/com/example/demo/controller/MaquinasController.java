package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.dto.Maquinas;
import com.example.demo.service.MaquinasServiceImpl;

@RestController
@RequestMapping("/api")
public class MaquinasController {


	@Autowired
	MaquinasServiceImpl maquinasServiceImpl;
	
	@GetMapping("/maquinas")
	public List<Maquinas> listarCmaquinas(){
		return maquinasServiceImpl.listarMaquinas();
	}
	
	
	@PostMapping("/maquinas")
	public Maquinas salvarMaquinas(@RequestBody Maquinas maquinas) {
		
		return maquinasServiceImpl.guardarMaquinas(maquinas);
	}
	
	
	@GetMapping("/maquinas/{id}")
	public Maquinas maquinasXID(@PathVariable(name="id") int id) {
		
		Maquinas Maquinas_xid= new Maquinas();
		
		Maquinas_xid=maquinasServiceImpl.maquinasXID(id);
		
		System.out.println("Maquinas XID: "+Maquinas_xid);
		
		return Maquinas_xid;
	}
	
	@PutMapping("/maquinas/{id}")
	public Maquinas actualizarMaquinas(@PathVariable(name="id")int id,@RequestBody Maquinas maquinas) {
		
		Maquinas Maquinas_seleccionado= new Maquinas();
		Maquinas Maquinas_actualizado= new Maquinas();
		
		Maquinas_seleccionado= maquinasServiceImpl.maquinasXID(id);
		
		Maquinas_seleccionado.setPiso(maquinas.getPiso());
		
		Maquinas_actualizado = maquinasServiceImpl.actualizarMaquinas(Maquinas_seleccionado);
		
		System.out.println("El Maquinas actualizado es: "+ Maquinas_actualizado);
		
		return Maquinas_actualizado;
	}
	
	@DeleteMapping("/maquinas/{id}")
	public void eleiminarMaquinas(@PathVariable(name="id")int id) {
		maquinasServiceImpl.eliminarMaquinas(id);
	}

	
}
