
CREATE TABLE `cajeros` (
  `codigo` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom_apels` varchar(255) DEFAULT NULL
);

INSERT INTO `cajeros` (nom_apels) VALUES ('Juan'),('Alberto'),('Antonia'),('Julia'),('Paco'),('John'),('Arnau'),('Alba'),('Sergi'),('Pedro');

CREATE TABLE `maquinas_registradoras` (
  `codigo` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `piso` int DEFAULT NULL
);

INSERT INTO `maquinas_registradoras` (piso) VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10);

CREATE TABLE `productos` (
  `codigo` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nombre` varchar(100) DEFAULT NULL,
  `precio` int DEFAULT NULL
);

INSERT INTO `productos` (nombre, precio) VALUES ('Leche',3),('Avena',2),('Cacao',2),('Avellanas',4),('Azucar',1),('Nocilla',3),('Queso',3),('Patatas',1),('Yogurt',2),('Pizza',2);

CREATE TABLE `venta` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `cajero` int NOT NULL,
  `maquina` int NOT NULL,
  `producto` int NOT NULL,
  FOREIGN KEY (`cajero`) REFERENCES `cajeros` (`codigo`),
  FOREIGN KEY (`maquina`) REFERENCES `maquinas_registradoras` (`codigo`),
  FOREIGN KEY (`producto`) REFERENCES `productos` (`codigo`)
);

INSERT INTO `venta` (cajero, maquina, producto) VALUES (1,1,1),(2,2,2),(3,3,3),(4,4,4),(5,5,5),(6,6,6),(7,7,7),(8,8,8),(9,9,9),(10,10,10);
