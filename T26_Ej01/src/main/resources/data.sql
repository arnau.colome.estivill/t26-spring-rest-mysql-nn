DROP TABLE IF EXISTS `piezas`;
DROP TABLE IF EXISTS `suministra`;
DROP TABLE IF EXISTS `proveedores`;

CREATE TABLE `piezas`(
	`codigo` INT NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(100),
    PRIMARY KEY (`codigo`)
);

INSERT INTO `piezas` (nombre) VALUES ('tornillo'), ('brocheta'), ('martillo'), ('taladro'), ('destornillador'), ('llave'), ('caja'), ('vaso'), ('alicates'), ('carraca');

CREATE TABLE `proveedores`(
	`id` CHAR(4) PRIMARY KEY,
    `nombre` VARCHAR(100)
);

INSERT INTO `proveedores` VALUES ('10', 'Martinez S.L.'), ('101', 'Joseisa'), ('29', 'Vinaixa'), ('38', 'Boni e hijos'), ('47', 'Maenju'), ('56', 'Jimaran'), ('65', 'Los albertos'), ('74', 'Olivemar'), ('83', 'Silvio'), ('92', 'Alegre');

CREATE TABLE `suministra` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `codigopieza` INT,
    `idproveedor` CHAR(4),
    `precio` INT,
    KEY (`codigopieza`, `idproveedor`),
    FOREIGN KEY (`codigopieza`) REFERENCES `piezas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`idproveedor`) REFERENCES `proveedores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO `suministra` (codigopieza, idproveedor, precio) VALUES (1, '10', 1), (2, '29', 10), (3, '38', 19), (4, '47', 68), (5, '56', 4), (6, '65', 12), (7, '74', 35), (8, '83', 3), (9, '92', 10), (10, '101', 20);

