package com.example.demo.dto;

import javax.persistence.*;

@Entity
@Table(name="suministra")//en caso que la tabala sea diferente
public class Suministra {

	//Atributos de entidad registro_piezas
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	private int id;

    @ManyToOne
    @JoinColumn(name = "codigopieza")
    private Piezas piezas;
    
  	@ManyToOne
    @JoinColumn(name = "idproveedor")
    private Proveedores proveedores;  
  	
	@Column(name = "precio")//no hace falta si se llama igual
	private int precio;
	
	
	//Constructores
	
	public Suministra() {
	
	}


	/**
	 * @param id
	 * @param pieza
	 * @param proveedor
	 * @param precio
	 */

	public Suministra(int id,Piezas piezas, Proveedores proveedores, int precio) {
		this.id = id;
		this.piezas = piezas;
		this.proveedores = proveedores;
		this.precio = precio;
	}


	//Getters y Setters
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the piezas
	 */
	public Piezas getPiezas() {
		return piezas;
	}

	/**
	 * @param piezas the piezas to set
	 */
	public void setPiezas(Piezas piezas) {
		this.piezas = piezas;
	}
	
	/**
	 * @return the proveedores
	 */
	public Proveedores getProveedores() {
		return proveedores;
	}


	/**
	 * @param proveedores the proveedores to set
	 */
	public void setProveedores(Proveedores proveedores) {
		this.proveedores = proveedores;
	}

	/**
	 * @return the precio
	 */
	public Integer getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	//Metodo impresion de datos por consola
	@Override
	public String toString() {
		return "PiezasProveedores [id=" + id + ", piezas=" + piezas + ", proveedores=" + proveedores + ", precio=" + precio + "]";
	}


}
