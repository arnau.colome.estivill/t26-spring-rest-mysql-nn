package com.example.demo.dto;

import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name="proveedores")//en caso que la tabala sea diferente
public class Proveedores {

	//Atributos de entidad curso
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	@Column(name = "id")//no hace falta si se llama igual
	private String id;
	@Column(name = "nombre")
	private String nombre;

	
	@OneToMany
    @JoinColumn(name="id")
    private List<Suministra> piezas;
	
	//Constructores
	
	public Proveedores() {
	
	}

	/**
	 * @param id
	 * @param nombre
	 * @param piezas
	 */
	public Proveedores(String id, String nombre, List<Suministra> piezas) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.piezas = piezas;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the piezasProveedores
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Suministra")
	public List<Suministra> getPiezas() {
		return piezas;
	}

	/**
	 * @param piezas the piezas to set
	 */
	public void setPiezas(List<Suministra> piezas) {
		this.piezas = piezas;
	}


	@Override
	public String toString() {
		return "Proveedores [id=" + id + ", nombre=" + nombre + "]";
	}


	
	
	
}
