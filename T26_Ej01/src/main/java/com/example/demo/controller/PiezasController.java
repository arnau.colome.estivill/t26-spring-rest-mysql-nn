package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.dto.Piezas;
import com.example.demo.service.PiezasServiceImpl;

@RestController
@RequestMapping("/api")
public class PiezasController {
	
	@Autowired
	PiezasServiceImpl piezasServiceImpl;
	
	@GetMapping("/piezas")
	public List<Piezas> listarPiezas(){
		return piezasServiceImpl.listarPiezas();
	}
	
	
	@PostMapping("/piezas")
	public Piezas salvarPiezas(@RequestBody Piezas piezas) {
		
		return piezasServiceImpl.guardarPiezas(piezas);
	}
	
	
	@GetMapping("/piezas/{codigo}")
	public Piezas piezasXID(@PathVariable(name="codigo") int id) {
		
		Piezas Piezas_xid= new Piezas();
		
		Piezas_xid=piezasServiceImpl.piezasXID(id);
		
		System.out.println("Piezas XID: "+Piezas_xid);
		
		return Piezas_xid;
	}
	
	@PutMapping("/piezas/{codigo}")
	public Piezas actualizarPiezas(@PathVariable(name="codigo")int id,@RequestBody Piezas piezas) {
		
		Piezas Piezas_seleccionado= new Piezas();
		Piezas Piezas_actualizado= new Piezas();
		
		Piezas_seleccionado= piezasServiceImpl.piezasXID(id);
		
		Piezas_seleccionado.setNombre(piezas.getNombre());
		
		Piezas_actualizado = piezasServiceImpl.actualizarPiezas(Piezas_seleccionado);
		
		System.out.println("La Piezas actualizada es: "+ Piezas_actualizado);
		
		return Piezas_actualizado;
	}
	
	@DeleteMapping("/piezas/{codigo}")
	public void eliminarPiezas(@PathVariable(name="codigo")int id) {
		piezasServiceImpl.eliminarPiezas(id);
	}
	
	
}
