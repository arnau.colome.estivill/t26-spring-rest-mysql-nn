package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Proveedores;


public interface IProveedoresService {

	//Metodos del CRUD
	public List<Proveedores> listarProveedores(); //Listar All 
	
	public Proveedores guardarProveedores(Proveedores proveedores);	//Guarda un Proveedores CREATE
	
	public Proveedores proveedoresXID(String id); //Leer datos de un Proveedores READ
	
	public Proveedores actualizarProveedores(Proveedores proveedores); //Actualiza datos del proveedores UPDATE
	
	public void eliminarProveedores(String id);// Elimina el proveedores DELETE


	
}
