package com.example.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IProveedoresDAO;
import com.example.demo.dto.Proveedores;

@Service
public class ProveedoresServiceImpl implements IProveedoresService{

	@Autowired
	IProveedoresDAO iProveedoresDAO;

	@Override
	public List<Proveedores> listarProveedores() {
		return iProveedoresDAO.findAll();
	}

	@Override
	public Proveedores guardarProveedores(Proveedores piezas) {
		return iProveedoresDAO.save(piezas);
	}

	@Override
	public Proveedores proveedoresXID(String id) {
		return iProveedoresDAO.findById(id).get();
	}

	@Override
	public Proveedores actualizarProveedores(Proveedores piezas) {
		return iProveedoresDAO.save(piezas);
	}

	@Override
	public void eliminarProveedores(String id) {
		iProveedoresDAO.deleteById(id);
	}
	
}
